from odoo import api, fields, models


class Job(models.Model):
    _name = 'cb.job'
    _description = 'Master Data Job'

    name = fields.Char(string='Job')
