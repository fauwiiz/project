from odoo import models, fields, api, _


class Customer(models.Model):
    _name = 'cb.customer'
    _inherits = {
        'res.partner': 'partner_id'
    }

    _GENDER = [
        ('laki-laki', 'Laki-Laki'),
        ('perempuan', 'Perempuan')
    ]

    nok = fields.Char(string='Nomer KTP', required=1)
    birth = fields.Date(string='Tanggal Lahir',required=1)
    partner_id = fields.Many2one('res.partner', string='Nasabah', required=1, ondelete='cascade')
    gender = fields.Selection(_GENDER, string='Jenis Kelamin', required=1)
    job_id = fields.Many2one('cb.job', string='Pekerjaan', required=1)
    education_id = fields.Many2one('cb.education', string='Pendidikan')
