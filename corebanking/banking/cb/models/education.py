from odoo import api, fields, models


class Education(models.Model):
    _name = 'cb.education'
    _description = 'Master Data Education'

    name = fields.Char(string='Education')
