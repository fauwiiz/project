from odoo import models,fields,api,_

class saving(models.Model):
    _name = 'cb.saving'
    _description = 'Saving Data'

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'
        self.name = self.env['ir.sequence'].next_by_code('cb.credit')

    @api.multi
    def action_paid(self):
        self.state = 'paid'

    # @api.multi
    # def action_accept(self):


    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
    
    @api.multi
    def action_refuse(self):
        self.state = 'refuse'

    _STATE = [
        ('draft','Draft'),
        ('confirm','Confirmed'),
        ('accept','Accepted'),
        ('refuse','Refused'),
        ('cancel','Cancelled'),
        ('save','Save'),        
    ]

    name = fields.Char(string='Code',required=1,readonly=True, default='/')
    partner_id = fields.Many2one('cb.customer', string='Nasabah',required=1)
    state = fields.Selection(_STATE,string='Status',required=1,readonly=1,default='draft')