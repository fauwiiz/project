from odoo import models,fields,api

class Category(models.Model):
    _name = 'cb.saving.category'
    _desciption = 'Saving Category'


    name = fields.Char(string='name', required=1)
    activation_deposit = fields.Float(string='Activation Deposit', required=1)
    cost_adm = fields.Float(string='Admin Cost', required=1)
    expired_day = fields.Integer(string='Expired', required=1)
    interest = fields.Float(string='Interest', required=1)
