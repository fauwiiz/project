from odoo import models,fields,api,_
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DT
from datetime import datetime,timedelta
import time
import logging
import monthdelta
from . import amort



class Credit(models.Model):
    _name = 'cb.credit'
    _description = 'Modul Untuk Credit nasabah'

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'
        self.name = self.env['ir.sequence'].next_by_code('cb.credit')

    @api.multi
    def action_paid(self):
        self.state = 'paid'

    @api.multi
    def action_accept(self):

        inv_obj = self.env['account.invoice']

        inv_vals = {}
        inv_vals['type'] = 'out_invoice'

        account_id = self.env.ref('l10n_generic_coa.1_conf_a_sale')

        self.date_approve = time.strftime(DT)

        for s in self: 
            logging.info("====== PARTNER ====== %s" % (s.partner_id.name)) 
            inv_vals['date_invoice'] = s.date_approve
            inv_vals['payment_term_id'] = s.period_id.id
            inv_vals['partner_id'] = s.partner_id.partner_id.id
            inv_vals['name'] = s.name
            inv_vals['invoice_line_ids'] = [(0,0,
            {'name':'Debt',
            'price_unit':s.amount,
            'quantity':1.0,
            'account_id':account_id.id
            }),
            (0,0,
            {'name':'Interest',
            'price_unit':s.amount * s.interest / 100,
            'quantity':1.0,
            'account_id':account_id.id
            })]


        invoice = inv_obj.create(inv_vals)
        invoice.action_invoice_open()
        self.state = 'accept'
        self.invoice_id = invoice.id

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
    
    @api.multi
    def action_refuse(self):
        self.state = 'refuse'
    
    # @api.multi
    # def action_

    _STATE = [
        ('draft','Draft'),
        ('cancel','Cancelled'),
        ('confirm','Confirmed'),
        ('refuse','Refused'),
        ('accept','Accepted'),
        ('paid','Paid'),        
    ]

    # _TENOR = [
    #     ('12','12'),
    #     ('24','24'),
    #     ('36','36'),
    #     ('48','48'),
    #     ('60','60'),
    #     ('72','72'),
    #     ('84','84'),
    #     ('96','96'),
    #     ('108','108'),
    #     ('120','120'),

    # ]

    _RATING  = [
        ('1','1'),
        ('2','2'),
        ('3','3'),
        ('4','4'),
        ('5','5'),

    ]

    @api.multi
    @api.depends('invoice_id.payment_ids')
    def _payment(self):
        for s in self:
            if s.invoice_id.payment_ids:
                s.payment_ids = s.payment_ids.ids

    def _currency(self):
        user = self.env['res.users'].browse(self._uid)
        return user.company_id.id
    
    # def action_generate(self):
    #     for s in self:
    #         ps = []
    #         amount = s.amount * (1+(s.interest / 100.0))
    #         total_months = s.period_id.months 
    #         for x in range(1,total_months):
    #             ps.append((0,0,{'amount':amount / total_months,'date':s.date_filling + x}))
                
    #         s.pay_ids = ps

    @api.multi
    def action_pay(self):
        pass

    @api.multi
    def action_gen_schedule(self):
        for s in self:
            rate = s.interest / 12
            years = (s.period_id.months / 12) * 12
            loan = amort.Loan(rate, years, s.amount,s.date_approve and s.date_approve or s.date_filling)
            print('------- LOAN ------', loan)
            periods = []
            for p in loan.schedule():
                #period, currentPeriod.interest, currentPeriod.principal, currentPeriod.balance
                print('-------',p)
                periods.append((0,0,{'date':p.date,'interest':p.interest,'principal':p.principal,'balance':p.balance}))
            s.sched_ids = periods


    name = fields.Char(string='Code',required=1,readonly=True, default='/')
    partner_id = fields.Many2one('cb.customer', string='Nasabah', required=1)
    date_filling = fields.Date(string='Tanggal Pengajuan', required=1, default=time.strftime(DT))
    date_approve = fields.Date(string='Tanggal Persetujuan', readonly=1)
    amount = fields.Float(string='Jumlah Pengajuan', required=1)
    residual = fields.Monetary(string='Residual', related='invoice_id.residual') 
    currency_id = fields.Many2one('res.currency',default=_currency,required=1)
    sched_ids = fields.One2many('cb.credit.schedule','credit_id',string='Payment List')
    payment_ids = fields.Many2many('account.payment',string='Payment', related='invoice_id.payment_ids')
    collateral_ids = fields.One2many('cb.collateral','credit_id', string='Collaterals')
    interest = fields.Float(string='Bunga',default=10.0,required=1)
    period_id = fields.Many2one('account.payment.term', string='Tenor', required=1, domain=[('tenor','=',True)])
    rating = fields.Selection(_RATING,string='Rating', required=1)
    invoice_id = fields.Many2one('account.invoice',string='Invoice', readonly=1)
    state = fields.Selection(_STATE,string='Status',required=1,readonly=1,default='draft')

class PaymentList(models.Model):
    _name = 'cb.credit.schedule'

    credit_id = fields.Many2one('cb.credit', string='Credit', required=1, readonly=1, ondelete='cascade')
    date = fields.Date(string='Date', required=1)
    principal = fields.Float(string='Amount')
    interest = fields.Float(string='Interest')
    balance = fields.Float(string='Balance')
    paid = fields.Boolean(string='Paid', default=False)

class Top(models.Model):
    _inherit = 'account.payment.term'

    tenor = fields.Boolean(string='Tenor', default=True)
    months = fields.Integer(string='Months')

    