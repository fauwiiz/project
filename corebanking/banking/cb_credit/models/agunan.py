from odoo import models,fields,api,_

class Agunan(models.Model):
    _name = 'cb.collateral'
    _description = 'Collateral Master Data'


    _TYPE = [
        ('1','Bergerak'),
        ('2','Tidak Bergerak'),
    ]

    credit_id = fields.Many2one('cb.credit', string='Credit', required=1, ondelete='cascade')
    picture = fields.Binary(string='Image')
    name = fields.Char(string='Name', required=1)
    type_name = fields.Selection(_TYPE,string='Type')
    place = fields.Char(string='Location')
    estimated = fields.Float(string='Estimated Price', required=1)
    

